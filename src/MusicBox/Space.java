package MusicBox;

import java.util.ArrayList;
import java.util.List;

public class Space {
    private int capacity;
    private List<Space> spaces;


    public Space(int capacity) {
        this.capacity = capacity;
        this.spaces = new ArrayList<Space>();
    }

    public int getCapacity() {
        return capacity;
    }

    public  int checkSpace() {
        int usedSpace = 0;
        for(Space space : spaces){
            usedSpace += space.getCapacity();
        }
        return capacity - usedSpace;

    }
}


