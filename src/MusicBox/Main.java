package MusicBox;

public class Main {
    public static void main(String[] args) {
        Song s1 = new Song(4,"IDK");
        Space sp1 = new Space(3);
        Disc d1 = new Disc(1,"Wäk","Kmp",15);
        Player p1 = new Player(d1,s1);
        MusicBox m1 = new MusicBox(d1,3,p1);

        m1.addDisc();
        m1.addDisc();
        m1.addDisc();
        m1.addDisc();
        m1.removeDisc();

        System.out.println(sp1.checkSpace());
    }

}
