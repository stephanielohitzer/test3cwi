package MusicBox;

public class Player {
    private Disc disc;
    private Song song;

    public Player(Disc disc,Song song) {
        this.disc = disc;
        this.song = song;
    }
}
