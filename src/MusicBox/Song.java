package MusicBox;

public class Song {
    private int length;
    private String name;

    public Song(int length, String name) {
        this.length = length;
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public String getName() {
        return name;
    }
}
