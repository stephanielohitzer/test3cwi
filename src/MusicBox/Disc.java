package MusicBox;

public class Disc {
    private int id;
    private String Song;
    private String name;
    private int playtime;



    public Disc(int id, String song, String name, int playtime) {
        this.id = id;
        Song = song;
        this.name = name;
        this.playtime = playtime;
    }
}
