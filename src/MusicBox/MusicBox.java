package MusicBox;

public class MusicBox {
    private Disc disc;
    private int space;
    private Player player;
    private int DiscNumber;

    public MusicBox(Disc disc, int space, Player player) {
        this.disc = disc;
        this.space = space;
        this.player = player;
    }

    public Disc addDisc(){
        System.out.println("Adding Disc");
        Disc disc = new Disc(1,"WÄH","Killmeplease",18 + DiscNumber);
        DiscNumber++;
        return disc;
    }

    public Disc removeDisc(){
        this.disc = disc;
        System.out.println("Removing Disc");
        return disc;
    }
}


